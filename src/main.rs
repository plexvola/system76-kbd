use std::{path::PathBuf, fs};
use clap::Parser;
use serde::Deserialize;
use toml;
use daemonize::Daemonize;

mod patterns;
mod leds;

#[derive(Parser)]
struct Cli {
    #[clap(parse(from_os_str), long, short, default_value = "/etc/system76-kbd/config.toml")]
    config: PathBuf,
    #[clap(long, short)]
    foreground: bool,
}

#[derive(Deserialize)]
struct Config {
    delay: u64,
    path: String,
    pattern: String,
    gradient: Vec<String>,
}

fn main() {
    let args = Cli::parse();
    let config: Config = toml::from_str(&fs::read_to_string(args.config).expect("Failed to read config file")).expect("Failed to parse config");

    if !args.foreground {
        let daemonize = Daemonize::new()
            .pid_file("/run/system76-kbd.pid")
            .chown_pid_file(true);

        daemonize.start().expect("Failed to move in the background");
    }

    let leds_path = PathBuf::from(config.path);
    let bp = leds_path.join("brightness");
    let cp = leds_path.join("color");

    match &config.pattern[..] {
        "fixed" => patterns::fixed(&cp, config.gradient[0].clone()),
        "rainbow" => patterns::rainbow(&cp, config.delay),
        "pulse" => patterns::pulse(&bp, &cp, config.delay, config.gradient[0].clone()),
        "cpu" => patterns::cpu(&cp, config.delay, config.gradient),
        "memory" => patterns::memory(&cp, config.gradient),
        s => eprintln!("{}: invalid pattern", s),
    }
}
