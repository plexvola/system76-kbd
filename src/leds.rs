use std::fs;
use std::path::Path;
use std::str::FromStr;
use palette::{Hsl, FromColor, Gradient, LinSrgb};

pub fn read_color(color_device: &Path) -> LinSrgb<u8> {
    let hex = fs::read_to_string(color_device).expect("Failed to read color device");
    LinSrgb::<u8>::from_str(hex.trim()).expect("Failed to parse color device RGB hex code")
}

pub fn write_color(color_device: &Path, color: LinSrgb<u8>) {
    fs::write(color_device, format!("{:X}", color)).expect("Failed to write color device");
}

pub fn shift_hue(color: LinSrgb<u8>, hue_amount: f32) -> LinSrgb<u8> {
    let mut huecolor = Hsl::from_color(color.into_format());
    huecolor.hue += hue_amount;
    LinSrgb::from_color(huecolor).into_format()
}

pub fn gen_gradient(colors: Vec<String>) -> Gradient<LinSrgb> {
    Gradient::new(colors.into_iter().map(
            |c| LinSrgb::from_str(&c).expect("Failed to parse gradient RGB hex code").into_format()
    ).collect::<Vec<LinSrgb>>())
}
