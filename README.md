# system76-kbd
System76 keyboard leds daemon.

This is a daemon to manage the leds colors for the System76 laptops keyboards.

It supports
- fixed color
- pulse animation
- rainbow animation
- animations based on CPU and memory usage
