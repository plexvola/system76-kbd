use std::path::Path;
use std::fs;
use std::{thread,time};
use psutil::cpu;
use psutil::memory;

use crate::leds;

pub fn memory(color_device: &Path, c: Vec<String>) {
	let mut _get_mem = || {
		let mem = memory::virtual_memory().expect("Failed to create VirtualMemory object");
		(mem.used() as f32)/(mem.total() as f32)
	};
	parametric(color_device, c, &mut _get_mem)
}

pub fn cpu(color_device: &Path, delay: u64, c: Vec<String>) {
	let mut cpu_p = cpu::CpuPercentCollector::new().expect("Failed to create CpuPercentCollector");
	let mut _get_cpu = || {
		thread::sleep(time::Duration::from_millis(delay));
		cpu_p.cpu_percent().expect("Failed to get CPU percentage") / 100.0
	};
	parametric(color_device, c, &mut _get_cpu)
}

pub fn pulse(brightness_device: &Path, color_device: &Path, delay: u64, color: String) {
	let sleep_delay = time::Duration::from_millis(delay);
	fs::write(color_device, color).expect("Failed to write to color device");
	loop {
		for b in 1..256 {
			fs::write(brightness_device, b.to_string()).expect("Failed to write to brightness device");
			thread::sleep(sleep_delay);
		}
		for b in (1..255).rev() {
			fs::write(brightness_device, b.to_string()).expect("Failed to write to brightness device");
			thread::sleep(sleep_delay);
		}
	}
}

pub fn fixed(color_device: &Path, color: String) {
	fs::write(color_device, color).expect("Failed to write to color device");
}

pub fn rainbow(color_device: &Path, delay: u64) {
	fs::write(color_device, "FF0000").expect("Failed to write initial rainbow color");
	let mut color = leds::read_color(color_device);
	let sleep_delay = time::Duration::from_millis(delay);
	loop {
		color = leds::shift_hue(color, 1.0);
		leds::write_color(color_device, color);
		thread::sleep(sleep_delay);
	}
}

pub fn parametric(color_device: &Path, c: Vec<String>, func: &mut dyn FnMut() -> f32) {
	let gradient = leds::gen_gradient(c);
	loop {
		let color = gradient.get(func());
		leds::write_color(color_device, color.into_format());
	}
}
